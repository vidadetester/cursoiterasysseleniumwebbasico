package siteIterasys;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Cursos {
	
	String url;
	WebDriver driver;
	
	//3.2 Metodos ou Funções
	@Before
	public void iniciar() {
		
		url = "https://www.iterasys.com.br";
		System.setProperty("webdriver.chrome.driver", "/home/devqa/Documents/Projetos/workspace-iterasys/siteIterasys/src/test/resources/drivers/linux/chromedriver");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60000, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
	}
	
	@After
	public void finalizar() {
		driver.quit();
	}
	
	@Test
	public void consultarCursos() {
		//Home - Página inicial
		driver.get(url); //Abrir o navegador na pagina indicada na url
		driver.findElement(By.id("searchtext")).clear();
		driver.findElement(By.id("searchtext")).sendKeys(Keys.chord("Mantis"));
		//Tirar o screenshot
		driver.findElement(By.id("searchtext")).sendKeys(Keys.ENTER);
		
		//Página que lista os cursos
		driver.findElement(By.cssSelector("span.comprar")).click();
		
		//Página do Carrinho de Compra
		
		//Resultado Esperado
		String titulo = "Mantis"; 
		String preco = "49,99";	
		
		assertEquals(titulo,driver.findElement(By.cssSelector("span.item-title")).getText());
		assertEquals(preco,driver.findElement(By.cssSelector("span.new-price")).getText());
				
		
	}

}
